import unittest


def unique(string):
    # Address case where there are more characters than in ASCII charset
    if len(string) > 128:
        return False

    # Keep bucket list of characters already used
    char_set = [False for _ in range(128)]
    for char in string:
        val = ord(char)  # Returns ASCII value of character
        if char_set[val]:
            return False
        char_set[val] = True

    return True  # All characters checked and no repeating ones


class Test(unittest.TestCase):
    dataT = ['abcd', 's4fad', '']
    dataF = ['23ds2', 'hb 627jh=j ()']

    def test_unique(self):
        for test_string in self.dataT:
            actual = unique(test_string)
            self.assertTrue(actual)
        for test_string in self.dataF:
            actual = unique(test_string)
            self.assertFalse(actual)


if __name__ == "__main__":
    unittest.main()
